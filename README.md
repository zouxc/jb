# JB

jb is `java book`，这里记录了一些我的技术摘要，本项目的目的力求分享精品Java技术干货。

如果你喜欢，`star` 便是，持续更新ing，还有Fork项目的同学，貌似没有什么卵用。。。

## 目录

* [开源组件实现](#开源组件实现)
	* [MVC框架实现篇](mvc/index.md)
	* [IOC容器实现篇](ioc/index.md)
	* ORM框架实现篇
	* AOP面向切面实现篇
	* 缓存实现篇
	* http服务器实现篇
	* 连接池实现篇
* [设计模式该怎么用](#设计模式该怎么用)
	* [如何正确地写出单例模式](designpatterns/singleton.md)
	* 代理
	* 装饰者
	* 观察者
	* 工厂
	* 享元
* [Java8系列](#Java8系列)
	* [Java8简明教程](java8/java8-guide.md)
	* [Java8 Foreach](java8/foreach.md)
	* Java8 Lambda
	* Java8 Stream
* [Hexo搭建博客](#Hexo搭建博客)
	* [分分钟部署一个Hexo环境](hexo/hello.md)
	* [各种配置详解](hexo/config.md)
	* [开始写作吧](hexo/writing.md)
* [用户指南](#用户指南)
	* [Jersey-2.x用户指南](https://waylau.gitbooks.io/jersey-2-user-guide/content/index.html)
	* [REST 实战](https://waylau.gitbooks.io/rest-in-action/content/)
	* [Java Servlet 3.1 规范](https://github.com/waylau/servlet-3.1-specification)
	* [MyBatis中文指南](http://mybatis.github.io/mybatis-3/zh/index.html)
	* [Apache Shiro 用户指南](https://github.com/waylau/apache-shiro-1.2.x-reference)
	* [Spring Boot参考指南](https://github.com/qibaoguang/Spring-Boot-Reference-Guide/blob/master/SUMMARY.md)
* [其它](#其它)
	* [git - 简明指南](git/guide.md) 
	* [linux安装jdk、tomcat脚本](shell/install_jdk_tomcat.sh)

### Contact

- Blog：https://biezhi.me
- Mail：biezhi.me@gmail.com

有兴趣分享技术的 可以发邮件给我！